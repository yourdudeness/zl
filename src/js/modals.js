
  const modal = $('.fancybox-slide');

function clearForm() {
  $('.form').trigger('reset');
  $('.form input, .form textarea').removeClass('input--invalid');
  $('.form__input-check, .form__input-error').hide();
  $('.form__input-outline').removeClass('input-outline--active');
  $('.form__upload').removeClass('upload--invalid');
}

//fancybox modal
  $('[data-fancybox]').fancybox({
    toolbar: false,
    smallBtn: true,
    touch: false,
    autoFocus:false,
    afterLoad: function () {
      let fancyboxSlide = document.querySelectorAll('.fancybox-slide');
      fancyboxSlide.forEach(function (element) {
        bodyScrollLock.disableBodyScroll(element);
      });
    },
    beforeClose: function () {
      if($('.fancybox-slide').length == 1) {
        bodyScrollLock.clearAllBodyScrollLocks();
        if (this.src !== '#modal-privacy') {
          clearForm();
          window.filesProject.length = 0;
          $('.form__upload-list').html('');
          $('.form__upload-text').show();
        }
      }
    },
  });

//show modal-vacancy content

  $('.vacancy-list__item .item__title-link, .vacancy-list__item .item__button').on('click', function () {
    let vacancyId = $(this).parents('.vacancy-list__item').attr('data-vacancy');
    let vacancyName = $(this).parents('.vacancy-list__item').find('.item__title-link').html();

    $('.modal--vacancy .modal__content').hide();
    $('.modal--vacancy .modal__content' + vacancyId).show();
    $('.modal--vacancy .modal__title-vacancy').html(vacancyName);
  });
