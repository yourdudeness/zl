$(document).ready(function () {

  const projectsSlider = $('.projects-block__slider');
  const projectsSliderCase = $('.block--case .projects-block__slider');
  const mainSlider = $('.main-screen__main-slider');
  const sliderNav = $('.slider-nav__timeline');
  const countSlides = $('.main-slider__slide').length;

  mainSlider.slick({
    slidesToShow: 1,
    autoplay: true,
    timeout: 4200,
    infinite: true,
    dots: false,
    arrows: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    pauseOnDotsHover: false,
  });


  $('.slider-nav__button.button--next').on('click', function() {
    mainSlider.slick('slickNext');
  });
  $('.slider-nav__button.button--prev').on('click', function() {
    mainSlider.slick('slickPrev');
  });

  //create list of nav elements

  for (var i = 0; i < countSlides; i++) {
    sliderNav.append('<li class="timeline__item" data-slide="'+ i +'">0'+(i + 1) +'</li>');
  }

  //add class first nav element
  sliderNav.find('.timeline__item[data-slide="0"]').addClass('item--current');

  //change slide on click
  sliderNav.find('.timeline__item').on('click', function () {
    var numSlide = $(this).attr('data-slide');
    mainSlider.slick('slickGoTo', numSlide);
  });

  //select current nav element
  mainSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    sliderNav.find('.timeline__item[data-slide="'+ currentSlide +'"]').removeClass('item--current');
    sliderNav.find('.timeline__item[data-slide="'+ nextSlide +'"]').addClass('item--current');
  });



  projectsSlider.slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    timeout: 4000,
    infinite: true,
    dots: false,
    arrows: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 680,
        settings: {
          slidesToShow: 1,
        }
      }
    ]

  });

  $('.slider-nav__button.button--next').on('click', function() {
    projectsSlider.slick('slickNext');
  });
  $('.slider-nav__button.button--prev').on('click', function() {
    projectsSlider.slick('slickPrev');
  });


});
