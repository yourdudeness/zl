$(document).ready(function () {
  //detecting user.

  if (localStorage.getItem('cookies') == null) {
    localStorage.setItem('cookies', 'true');

    let cookies = $('.popover-cookies');
    let closeBtn = $('.popover-cookies__button-close');
    let agreeBtn = $('.popover-cookies__button-agree');

    setTimeout(showCookies, 1000);

    closeBtn.add(agreeBtn).on('click', function () {
      cookies.removeClass('active');
    });

    function showCookies() {
      cookies.show();
      setTimeout(function () {
        cookies.addClass('active');
      }, 500);
    }
  }

});
