$(document).ready(function () {
  const form = $('.form');

  window.filesProject = [];
  let filesResume = [];
  let errTel = false;
  let errEmail = false;
  let errFiles = false;

  let dropArea = $('.form__upload');

  //validate input-fields
  const input = $('.form__input, .form__textarea');
  input.on('focus', function () {
    $(this).parent().addClass('input-outline--active');
    $(this).removeClass('input--invalid');
    $(this).parent().find('.form__input-error').hide();
  });

  input.on('focusout', function () {
    $(this).parent().removeClass('input-outline--active');
  });

  input.on('input', function () {
    if ($(this).hasClass('input-tel')) {  //Проверка полного номера

        let countNum = 0;
        let str = $(this).val();
        for (var i = 0; i < str.length; i++) {
          if (!isNaN(str[i]) && str[i] !== ' ') {
            countNum++;
          }
        }
        if (countNum == 11) {
          errTel = false;
          $(this).parent().find('.form__input-check').show();
        } else {
          errTel = true;
          $(this).parent().find('.form__input-check').hide();
        }

    } else if ($(this).hasClass('input-email')) {  // Проверка Правильности email

        let email = $(this).val();
        if ((email.match(/.+?\@.+/g) || []).length !== 1) {
          errEmail = true;
          $(this).parent().find('.form__input-check').hide();
        } else {
          errEmail = false;
          $(this).parent().find('.form__input-check').show();
        }

        if (!$(this).hasClass('req-field')) {
          errEmail = false;
        }

    } else if ($(this).val() !== '') {
      $(this).parent().find('.form__input-check').show();
    } else {
      $(this).parent().find('.form__input-check').hide();
    }
  });


   //  ============= Загрузка файлов ==============

  // check type upload file
  function loadMime(file, mimes,  callback) {
    function check(bytes, mime) {
      for (var i = 0, l = mime.mask.length; i < l; ++i) {
        if ((bytes[i] & mime.mask[i]) - mime.pattern[i] !== 0) {
          return false;
        }
      }
      return true;
    }

    var blob = file.slice(0, 4); //read the first 4 bytes of the file
    var reader = new FileReader();
    reader.onloadend = function (e) {
      if (e.target.readyState === FileReader.DONE) {
        var bytes = new Uint8Array(e.target.result);
        for (var i = 0, l = mimes.length; i < l; ++i) {
          if (check(bytes, mimes[i])) {
            return callback(true);
          }
        }
        return callback(false);
      }
    };
      reader.readAsArrayBuffer(blob);
  }

  const projectUpload = document.querySelector('#file-project');
  const resumeUpload = document.querySelector('#file-resume');

  if ($(projectUpload).length) {
    $(projectUpload).on('change', function() {
      uploadFiles(this, window.filesProject);
    });
  }

  if ($(resumeUpload).length) {
    $(resumeUpload).on('change', function() {
      uploadFiles(this, filesResume);
    });
  }

  // drug oan drop
  dropArea.add($('body')).on('dragenter dragover dragleave drop', function (e) {
    e.preventDefault();
    e.stopPropagation();
  });
  dropArea.on('dragenter dragover', function() {
    highlight($(this));
  });
  dropArea.on('dragleave drop', function() {
    unhighlight($(this));
  });

  dropArea.on('drop', function(e) {
    let filesArray = ($(e.currentTarget).closest('.form__upload').hasClass('upload--resume')) ? filesResume : window.filesProject;
    let upload = $(e.currentTarget).find('.form__input-upload');
    upload.files = e.originalEvent.dataTransfer.files;
    uploadFiles(upload, filesArray);
  });

  function highlight(elem) {
    elem.addClass('highlight');
  }
  function unhighlight(elem) {
    elem.removeClass('highlight');
  }

  function uploadFiles(upload, filesArray) {
          //List mimes
      let mimes = [
        {
          mime: 'application/pdf',
          pattern: [0x25, 0x50, 0x44, 0x46],
          mask: [0xFF, 0xFF, 0xFF, 0xFF],
        },
        {
          mime: 'application/msword',
          pattern: [0xD0, 0xCF, 0x11, 0xE0],
          mask: [0xFF, 0xFF, 0xFF, 0xFF],
        },
        {
          mime: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
          pattern: [0x50, 0x4B, 0x3, 0x4],
          mask: [0xFF, 0xFF, 0xFF, 0xFF],
        },

      ];

      $(upload).parent().removeClass('upload--invalid'); //clear error
      $(upload).parent().find('.form__input-error').hide();

      errFiles = false;
      let fileList = $(upload).parent().find('.form__upload-list');
      for (let i = 0; i < upload.files.length; i++) {
        let fileExec = /[^.]+$/.exec(upload.files[i].name);
        if (fileExec == 'pdf' || fileExec == 'doc' || fileExec == 'docx') {
          loadMime(upload.files[i], mimes, function(result) {
            if (result) {
              filesArray.push(upload.files[i]);
              let html = '<li class="form__upload-file">' + upload.files[i].name +
                '<button class="form__upload-file-remove" data-file-index="'+ (filesArray.length - 1) +
                '">Удалить файл</button></li>';
              fileList.show().append(html);
              $(upload).parent().find('.form__upload-text').hide();
            }
          });
        } else {
           errFiles = true;
           $(upload).parent().addClass('upload--invalid');
           $(upload).parent().find('.form__input-error').show();
        }
      }
  }

  //удаление файла
  $('.form__upload-list').on('click','.form__upload-file-remove', function (e) {
    e.preventDefault();
    let uploadArea = $(this).closest('.form__upload');
    let fileList = uploadArea.find('.form__upload-list');
    let filesArray = ($(this).closest('.form__upload').hasClass('upload--resume')) ? filesResume : window.filesProject;

    let indexFile = $(this).attr('data-file-index');
    filesArray.splice(indexFile, 1);   //удаляем элемент c пересчетом индексов

    fileList.html('');
    for (let i = 0; i < filesArray.length; i++) {
    let html = '<li class="form__upload-file">' + filesArray[i].name +
               '<button class="form__upload-file-remove" data-file-index="'+ i +
               '">Удалить файл</button></li>';
      fileList.append(html);
    }
    uploadArea.removeClass('upload--invalid');
    uploadArea.find('.form__input-error').hide();

    if(!filesArray.length) {
      $('.form__upload-text').show();
    }

  });


   //  ============= /Загрузка файлов ==================

  //  ============== Отправка формы ====================
  form.on('submit', function (e) {
    e.preventDefault();

    //Check validate input
    $(this).find('.req-field').each(function () {

      $(this).removeClass('input--invalid');

      //Проверка правильности ввода телефона
      if ($(this).hasClass('input-tel')) {
        if (errTel) {
          $(this).addClass('input--invalid');
          $(this).parent().find('.form__input-error').html('Введен неверный номер телефона');
          $(this).parent().find('.form__input-error').show();
        }
      }

      if ($(this).hasClass('input-email')) {
        if (errEmail) {
          $(this).addClass('input--invalid');
          $(this).parent().find('.form__input-error').html('Введен неверный e-mail ');
          $(this).parent().find('.form__input-error').show();
        }
      }

      if ($(this).val() === '') {
        $(this).addClass('input--invalid');
        $(this).parent().find('.form__input-error').html('Поле обязательно для заполнения');
        $(this).parent().find('.form__input-error').show();
      }
    });

    let errResumeFiles = false;
    let formUpload = $(this).find('.form__upload');
    let attachedFiles = [];

    if (formUpload.length) {    //форма с загрузкой файлов
      formUpload.removeClass('upload--invalid');
      formUpload.find('.form__input-error').hide();

      if (formUpload.hasClass('upload--resume')) {
        attachedFiles = filesResume;
        if (formUpload.hasClass('requared')) {
          if (!attachedFiles.length) {
            errResumeFiles = true;
            formUpload.addClass('upload--invalid');
            formUpload.find('.form__input-error').show();
          }
        }
      } else {
        attachedFiles = window.filesProject;
      }
    }

    //Меняем содержимое модального окна .modal--thanks
    var modalContentID = '#thanks-default';

    if ($(this).hasClass('tell-us-block__contact-form')) {
      modalContentID = '#thanks-review';
    } else if ($(this).hasClass('resume-block__resume-form')) {
      modalContentID = '#thanks-vacancy';
    }
    $('.modal--thanks .modal__content').hide();
    $(modalContentID).show();

    var countEmpty = $(this).find('.input--invalid').length;

    if (countEmpty > 0 || errTel || errEmail || errResumeFiles) {
      return false
    } else {

      let data = new FormData($(this)[0]);

      if (attachedFiles.length) {
        for (let i = 0; i < attachedFiles.length; i++) {
          data.append('file'+ i, attachedFiles[i]);
        }
      }

      var url = $(this).attr('action');

      //=========Ajax============
      $.ajax(url, {
          method: 'post',
          data: data,
          processData: false,
          success: function () {

          }
      });

      console.log('message was send');
      //=========================

      //close current modal
      $.fancybox.close();
      //open modal thanks
      $.fancybox.open({
          src: '#modal-thanks',
          opts: {
            afterLoad: function () {
              let fancyboxSlide = document.querySelectorAll('.fancybox-slide');
              fancyboxSlide.forEach(function (element) {
                bodyScrollLock.disableBodyScroll(element);
              });
            },
            beforeClose: function () {
              bodyScrollLock.clearAllBodyScrollLocks();
            }
          }
        },
        {
          autoFocus: false,
          touch: false,
        });

      //Очищаем формы после отправки
      clearForm($(this));
      attachedFiles.length = 0;
      filesResume.length = 0;
      window.filesProject.length = 0;
      $('.form__upload-list').html('');
      $('.form__upload-text').show();
    }
  });

  //  ============== /Отправка формы ====================

  //очитка формы
  function clearForm(elem) {
    form.trigger('reset');
    elem.find('.form__input-check').hide();
  };

  //phone input mask+
  var phone = document.querySelectorAll('.input-tel');
  phone.forEach(function (element) {
    var phoneMask = IMask(
      element, {
        mask: '+{7}(000)000-00-00',
        placeholderChar: '_',
        lazy: false,
        overwrite: false,
      });
  });

  //Clear contact form and checkbox
  form.trigger('reset');



});
