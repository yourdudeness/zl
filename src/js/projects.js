$(document).ready(function () {

  const lastItem = $('.projects-block__project-item.project-item--last');
  const projectItems = $('.projects-block__project-item:not(.project-item--first)');
  const moreBtn = $('.projects-block__show-more-button');
  const tagProjects = $('.filter-item__link');
  let countItem = projectItems.length;
  let countToShow = 12;
  let filter = 'all';

  //скрываем фильтр если нет элементов с таким же классом
  $(tagProjects).each(function() {
    let filter = $(this).attr('data-filter');
    if(!$('.projects-block__slide'+ filter).length && !$('.projects-block__project-item'+ filter).length  && filter !== 'all') {
      $(this).hide();
    }
  });

  //projects filter
 tagProjects.on('click', function (e) {
     e.preventDefault();
     $(tagProjects).removeClass('link--active');
     $(this).addClass('link--active');

     filter = $(this).attr('data-filter');
     filterToSelect = (filter !== 'all') ? filter : '';

     let slider = $('.projects-block__slider');
     let projectsBlock = $('.projects-block__project-list');

     if (slider.length) {   //если фильтр работает на слайдере
       if (filter === 'all') {
         slider.slick('slickUnfilter');
         slider.slick('slickGoTo', 0);
       } else {
         slider.slick('slickUnfilter');
         slider.slick('slickFilter', filter);
         slider.slick('slickGoTo', 0);
       }
     }

     if (projectsBlock.length) {  //если фильтр работает на блоке проектов
        if (filter === 'all') {
          $('.projects-block__project-item.item--visible').show();
        } else {
          $('.projects-block__project-item:not(.project-item--last)').hide();
          $('.projects-block__project-item.item--visible' + filter).filter(filter).show();
        }
     }

     showLastBlock();
     getSmallBlock();
     disableMoreBtn()
   });


  //Показываем / скрываем посл. блок
  function showLastBlock() {
    let countvisibleItems = $('.projects-block__project-item:visible:not(.project-item--last)' ).length;
    if (countvisibleItems % 2 === 0 ) {
      lastItem.removeClass('item--visible');
    } else {
      lastItem.addClass('item--visible');
    }
  }

  //Показываем или скрываем кнопку
  showMoreBtn();
  function showMoreBtn() {
    if (countItem > 13) {
      moreBtn.show();
    } else {
      moreBtn.hide();
    }
  }

  function disableMoreBtn() {
    let hideItems = $('.projects-block__project-item' + filterToSelect + ':hidden:not(.project-item--first, .project-item--last)');
    if (hideItems.length === 0) {
      moreBtn.attr('disabled', true);
    } else {
      moreBtn.removeAttr('disabled');
    }
  }

  //Load More Projects
  moreBtn.on('click', function (e) {
    e.preventDefault();
    let hideItems = $('.projects-block__project-item' + filterToSelect + ':hidden:not(.project-item--first, .project-item--last)');
    showMore(hideItems);
    disableMoreBtn();
  });

  let filterToSelect = (filter !== 'all') ? filter : '';
  let hideElems = $('.projects-block__project-item' + filterToSelect + ':hidden:not(.project-item--first, .project-item--last)');

  //show on load page
  showMore(hideElems);

  //show on button click
  function showMore(hideItems) {
    hideItems.slice(0, countToShow).slideDown({
      start: function () {
        $(this).css({
          display: 'flex'
        });
        $(this).addClass('item--visible');
      }
    }, 400);
    showLastBlock();
    getSmallBlock();
  }


  getSmallBlock();  // при загрузке страницы

  function getSmallBlock() {
    projectItems.removeClass('item--small');
    let projects = $('.projects-block__project-item:first-child, .projects-block__project-item:visible');
    projects.each(function () {
      let projectIndex = projects.index($(this));

      for (let i=3; i<=4; i++) {
        for (let j=0; j<30; j++) {  // кол-во проектов для сравнения = 4*30 + 4
          if (projectIndex + 1 === 4*j + i) {
            $(this).addClass('item--small');
          }
        }
      }
    });
  };

});
