$(window).on('load', function () {
  var markerPath = $('#map').attr('data-marker');
  var map = document.querySelector('#map');
  var width = $(window).width();
  var mapCenter = [56.296420, 44.065189];

  if (width <= 640) {
    mapCenter = [56.298304, 44.065552];
  } else if (width <= 992) {
    mapCenter = [56.298146, 44.059628];
  }

  var onIntersection = function onIntersection(entries) {
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = entries[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var entry = _step.value;

        if (entry.intersectionRatio > 0) {
          ymaps.ready(mapsInit);
          observer.unobserve(entry.target);
        }
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return != null) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  };

  if ($('#map').length) {
    var observer = new IntersectionObserver(onIntersection);
    observer.observe(map);
  }

  function mapsInit() {
    myMap = new ymaps.Map('map', {
      center: mapCenter,
      zoom: 15,
      controls: ['zoomControl']
    });
    var Place = new ymaps.Placemark([56.296372, 44.065237], {
      balloonContent: ''
    }, {
        hasBalloon: false,
        iconLayout: 'default#image',
        iconImageHref: markerPath,
        iconImageSize: [50, 65],
        iconImageOffset: [-25, -65],
      });
    myMap.geoObjects.add(Place);
    myMap.behaviors.disable('scrollZoom');
  };
});







