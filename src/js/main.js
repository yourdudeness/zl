$(document).ready(function () {

  //team tabs
  $('.team-block__tab-panel-item').on('click', function () {
    if ($(window).width() > 576 ) {
      let tabID = $(this).attr('data-tab');

      $('.team-block__tab-panel-item').removeClass('item--active');
      $(this).addClass('item--active');

      //select-tab
      $('.team-block__tab').removeClass('tab--active');
      $('#' + tabID).addClass('tab--active')
    }
  });

  // Hide block vacancy if vacancies was not found
  if (!$('.vacancy-list__item').length) {
    $('.vacancy-block').hide();
  }

  //resume tabs
  const tab = $('.resume-form__tab');
  const tabBtn = $('.tab-panel__button');

  tabBtn.on('click', function (e) {
    e.preventDefault();
    let tabID = $(this).attr('data-tab');
    let currentTab = $(tabID);

    if (!currentTab.is(":visible") ) {
      tabBtn.removeClass('button--active');
      $(this).addClass('button--active');

      tab.hide();
      tab.find('.form__input').removeClass('req-field input--invalid').val('');
      tab.find('.form__input-error').hide();
      tab.find('.form__input-check').hide();
      tab.find('.form__upload').removeClass('requared upload--invalid');

      currentTab.show();
      currentTab.find('.form__input').addClass('req-field');
      currentTab.find('.form__upload').addClass('requared');
    }

  });

  //main-menu
  const menuBtn = $('.header__menu-caller');
  const menuContactBtn = $('.header__callback-button');
  const contactMenu = $('.modal-contacts-menu');
  const mainMenu = $('.modal-menu');
  const header = $('.header');
  const anchorLink = $('.link--anchor');

  mainMenu.show();
  contactMenu.show();

  function openMenu() {
    if (contactMenu.hasClass('menu--active')) {
      closeContactMenu();
    }
    menuBtn.addClass('caller--active');
    menuContactBtn.addClass('btn--invert');
    mainMenu.addClass('menu--active');
    header.addClass('header--active');
    $('.header__logo-img').attr('src','/img/logo-black.png');
    bodyScrollLock.disableBodyScroll(document.querySelector('.modal-menu'));
  }

  function closeMenu() {
    menuBtn.removeClass('caller--active');
    menuContactBtn.removeClass('btn--invert');
    mainMenu.removeClass('menu--active');
    header.removeClass('header--active');
    if (header.hasClass('header--cases')) {
      $('.header__logo-img').attr('src','/img/logo-white.png');
    } else {
      $('.header__logo-img').attr('src','/img/logo.png');
    }
    bodyScrollLock.enableBodyScroll(document.querySelector('.modal-menu'));
  }

  function openContactMenu () {
    if (mainMenu.hasClass('menu--active')) {
      closeMenu();
    }
    menuContactBtn.addClass('btn--active');
    contactMenu.addClass('menu--active');
    menuBtn.addClass('caller--invert');
    header.addClass('header--active');
    $('.header__logo-img').attr('src','/img/logo-black.png');
    bodyScrollLock.disableBodyScroll(document.querySelector('.modal-contacts-menu'));
  }

  function closeContactMenu () {
    menuContactBtn.removeClass('btn--active');
    contactMenu.removeClass('menu--active');
    menuBtn.removeClass('caller--invert');
    header.removeClass('header--active');
    if (header.hasClass('header--cases')) {
      $('.header__logo-img').attr('src','/img/logo-white.png');
    } else {
      $('.header__logo-img').attr('src','/img/logo.png');
    }
    bodyScrollLock.enableBodyScroll(document.querySelector('.modal-contacts-menu'));
  }

  menuBtn.on('click', function () {
    if (mainMenu.hasClass('menu--active')) {
      closeMenu();
    } else {
      openMenu();
    }
  });


  (function goToAnchor() {
    hash = document.location.hash;
    if (hash !="") {
        setTimeout(function() {
            if (location.hash) {
                window.scrollTo(0, 0);
                window.location.href = hash;
            }
        }, 1);
    }
    else {
      return false;
    }
  })();

  //Scroll to anchor
  anchorLink.on('click', function (e) {
    e.preventDefault();
    closeMenu();

    let anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: $(anchor.attr('href')).offset().top
      }, 400);
  });

  $(document).mouseup(function (e) {
    if ($(window).width() > 1024 ) {
      if (!mainMenu.is(e.target)
        && mainMenu.has(e.target).length === 0
        && header.has(e.target).length === 0) {
        closeMenu();
      }
    }
	});

  //contacts menu
  menuContactBtn.on('click' , function () {
    if (contactMenu.hasClass('menu--active')) {
      closeContactMenu();
    } else {
      openContactMenu();
    }
  });

  //close menu on button click
  $('.main-menu__order-button, .header__start-project-button, .list__item-button').on('click', function () {
    closeMenu();
    closeContactMenu();
  });

});
