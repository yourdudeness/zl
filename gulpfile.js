//Подключаем модули галпа
const gulp = require('gulp'),
      sass = require('gulp-sass'),
      concat = require('gulp-concat'),
      autoprefixer = require('gulp-autoprefixer'),
      cleanCSS = require('gulp-clean-css'),
      terser = require('gulp-terser'),
      del = require('del'),
      imagemin = require('gulp-imagemin'),
      htmlmin = require('gulp-htmlmin'),
      imageminJpegRecompress = require('imagemin-jpeg-recompress'),
      touch = require('gulp-touch-fd'),
      critical = require('critical').stream,
      browserSync = require('browser-sync').create(),
      sourcemaps = require('gulp-sourcemaps');

const stylesFiles = [
   './src/sass/**/*.scss'
];

const jsFiles = [
   './src/js/jquery-3.4.0.min.js',
   './src/js/**/*.js',
];

const imgFiles = [
  './src/img/**/*.jpg',
  './src/img/**/*.gif',
  './src/img/**/*.svg'
];

//Таск на стили CSS
function styles() {

  return gulp.src(stylesFiles)
   .pipe(sourcemaps.init())
   .pipe(sass({
     includePaths: require('node-normalize-scss').includePaths
    }))

   .pipe(concat('styles.css'))
   .pipe(autoprefixer({
     browserslistrc: ['last 2 versions'],
     cascade: false
   }))
   .pipe(cleanCSS({
     level: 2
   }))

   .pipe(sourcemaps.write('.'))
   .pipe(sourcemaps.write())
   .pipe(gulp.dest('./build/css'))
   .pipe(browserSync.stream());
}

// Generate & Inline Critical-path CSS
function criticalcss() {
  return gulp.src('build/**/*.html')
    .pipe(critical({
      base: 'dist/',
      inline: true,
      css: ['build/css/styles.css'],
      ignore: ['background','@font-face'],
    }))
    .on('error', function (err) {
      log.error(err.message);
    })
    .pipe(gulp.dest('build'));
}



function scripts() {
   return gulp.src(jsFiles)
   .pipe(sourcemaps.init())
   .pipe(concat('scripts.js'))
   .pipe(terser())

   .pipe(sourcemaps.write())
   .pipe(gulp.dest('./build/js'))
   .pipe(browserSync.stream())
   .pipe(touch());
}

function minimg() {
   return gulp.src(imgFiles)
   .pipe(
        imagemin([
          imagemin.jpegtran({ progressive: true }),
          imagemin.optipng({ optimizationLevel: 5 }),
          imageminJpegRecompress({
             loops: 3,
             min: 75,
             max: 80,
             quality:'high'
           }),
          imagemin.svgo(),
          ]),
      )
   .pipe(gulp.dest('./build/img'))
}

function png() {
   return gulp.src('./src/img/**/*.png')
   .pipe(gulp.dest('./build/img'))
}

function minhtml() {
  return gulp.src('./src/**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./build'));
}

function fonts() {
  return gulp.src('./src/fonts/**/*')
    .pipe(gulp.dest('./build/fonts'));
}
function video() {
  return gulp.src('./src/video/**/*')
    .pipe(gulp.dest('./build/video'));
}

function clean() {
   return del(['build/css/**/*.css'])
}

function watch() {
   browserSync.init({
      server: {
          baseDir: "./build/",
          index: "index.html"
      }
  });

  gulp.watch('./src/sass/**/*.scss', styles);
  gulp.watch('./src/sass/css/**/*.css').on('change', browserSync.reload);
  gulp.watch(['./src/js/**/*.js', '!./src/js/scripts.js'], scripts);
  gulp.watch('./src/**/*.html').on('change', gulp.series(minhtml, browserSync.reload));
}

exports.styles = styles;
exports.inlinecss = criticalcss;
exports.scripts = scripts;
exports.delcss = clean;
exports.images = minimg;
exports.png = png;
exports.html = minhtml;
exports.fonts = fonts;
exports.video = video;
exports.watch = watch;
exports.build = gulp.series(clean, gulp.parallel(fonts, video, minhtml, styles, scripts, minimg, png), criticalcss);
exports.dev = gulp.series(exports.build, watch);
